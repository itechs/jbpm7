package com.holi.collections;

import java.util.List;
import java.util.NoSuchElementException;

public class Lists {
    public static <T> T single(List<T> list) {
        switch (list.size()) {
            case 1:
                return list.get(0);
            case 0:
                throw new NoSuchElementException("Empty list");
            default:
                throw new IllegalArgumentException("List consist of many elements: " + list.size());

        }
    }
}
