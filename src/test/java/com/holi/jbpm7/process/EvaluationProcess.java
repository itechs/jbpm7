package com.holi.jbpm7.process;

import com.google.common.collect.ImmutableMap;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.process.CorrelationAwareProcessRuntime;
import org.kie.internal.process.CorrelationKey;

import java.util.Map;
import java.util.Objects;

public class EvaluationProcess {
    private KieSession session;

    private EvaluationProcess(KieSession session) {
        this.session = Objects.requireNonNull(session);
    }

    public ProcessInstance startProcessFor(String userId, String reason) {
        return startProcessFor(userId, reason, null);
    }

    public ProcessInstance startProcessFor(String userId, String reason, CorrelationKey key) {
        return ((CorrelationAwareProcessRuntime) session).startProcess("com.sample.evaluation", key, startingParametersMap(userId, reason));
    }

    private static Map<String, Object> startingParametersMap(String userId, String reason) {
        return ImmutableMap.of("employee", Objects.requireNonNull(userId, "process assign to user can't be null!"), "reason", reason);
    }

    public static EvaluationProcess of(KieSession session) {
        return new EvaluationProcess(session);
    }
}
