package com.holi.jbpm7;

import com.holi.jbpm7.process.EvaluationProcess;
import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.runtime.manager.context.EmptyContext;
import org.kie.internal.runtime.manager.context.ProcessInstanceIdContext;

import java.util.concurrent.*;

import static com.holi.jbpm7.Users.krisv;
import static org.hamcrest.Matchers.*;
import static org.jbpm.test.JbpmJUnitBaseTestCase.Strategy.*;
import static org.junit.Assert.assertThat;

public class RuntimeManagerStrategyTest extends JbpmJUnitBaseTestCase {

    private static final String USE_DEFAULT_IDENTIFIER = null;

    public RuntimeManagerStrategyTest() {
        super(true, true);
    }

    @Override
    public void setUp() throws Exception {
        activeEngines = new CopyOnWriteArraySet<>();
        customHandlers = new ConcurrentHashMap<>();
        super.customEnvironmentEntries = new ConcurrentHashMap<>();
        super.customAgendaListeners = new CopyOnWriteArrayList<>();
        super.customProcessListeners = new CopyOnWriteArrayList<>();
        super.customTaskListeners = new CopyOnWriteArrayList<>();
        super.setUp();
    }

    @Test
    public void shareSessionsInApplicationViaSingletonStrategy() throws InterruptedException, ExecutionException, TimeoutException {
        createRuntimeManager(SINGLETON, USE_DEFAULT_IDENTIFIER);

        assertThat(directSession(), is(sameInstance(directSession())));
        assertThat(requestSession(), is(sameInstance(requestSession())));
    }

    @Test
    public void createNewlySessionForRequestViaPerRequestStrategy() throws InterruptedException, ExecutionException, TimeoutException {
        createRuntimeManager(REQUEST, USE_DEFAULT_IDENTIFIER);

        assertThat(directSession(), is(sameInstance(directSession())));
        assertThat(requestSession(), is(not(sameInstance(directSession()))));
    }

    @Test
    @Ignore("Can't initializing TimerService correctly")
    public void createSessionForEachProcessInstanceViaPerProcessInstanceStrategy() throws InterruptedException, ExecutionException, TimeoutException {
        createRuntimeManager(PROCESS_INSTANCE, USE_DEFAULT_IDENTIFIER, "Evaluation.bpmn");
        assertThat(requestSession(), is(not(requestSession())));

        KieSession session = directSession();
        ProcessInstance pi = EvaluationProcess.of(session).startProcessFor(krisv, "anything");

        assertProcessInstanceActive(pi.getId());
        assertThat(requestSession(pi.getId()), is(sameInstance(session)));
    }

    private KieSession requestSession() throws InterruptedException, ExecutionException, TimeoutException {
        return requestSession(null);
    }

    private KieSession requestSession(Long processInstanceId) throws InterruptedException, ExecutionException, TimeoutException {
        return CompletableFuture.supplyAsync(() -> directSession(processInstanceId)).get(3, TimeUnit.SECONDS);
    }

    public KieSession directSession() {
        return directSession(null);
    }

    public KieSession directSession(Long processInstanceId) {
        return getRuntimeEngine(processInstanceId == null ? EmptyContext.get() : new ProcessInstanceIdContext(processInstanceId)).getKieSession();
    }

}
