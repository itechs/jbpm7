package com.holi.jbpm7;

import com.google.common.collect.ImmutableMap;
import com.holi.jbpm7.process.EvaluationProcess;
import com.holi.jbpm7.task.TaskSummarySnapshot.TaskSummarySnapshotBuilder;
import org.hamcrest.Matcher;
import org.jbpm.persistence.correlation.CorrelationKeyInfo;
import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.event.KieRuntimeEvent;
import org.kie.api.event.process.ProcessEvent;
import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.event.process.ProcessNodeEvent;
import org.kie.api.event.process.ProcessVariableChangedEvent;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.TaskSummary;
import org.kie.internal.process.CorrelationAwareProcessRuntime;
import org.kie.internal.process.CorrelationKey;
import org.kie.internal.runtime.manager.context.EmptyContext;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.io.Files.asCharSource;
import static com.holi.collections.Lists.single;
import static com.holi.jbpm7.Users.*;
import static com.holi.jbpm7.process.CorrelationKeys.correlationKey;
import static com.holi.jbpm7.process.CorrelationKeys.searchBasedCorrelaionKey;
import static com.holi.jbpm7.task.TaskSummarySnapshot.as;
import static com.holi.jbpm7.task.TaskSummarySnapshot.snapshotOf;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.kie.api.task.model.Status.*;

public class EvaluationProcessTest extends JbpmJUnitBaseTestCase {

    private TaskService taskService;
    private KieSession session;

    public EvaluationProcessTest() {
        super(true, true);
        super.getInMemoryLogger();
    }

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        createRuntimeManager("Evaluation.bpmn");
        RuntimeEngine engine = getRuntimeEngine(EmptyContext.get());
        taskService = engine.getTaskService();
        session = engine.getKieSession();
        getLogService();
    }

    @Test
    public void completeStartedProcess() {
        String reason = "Yearly performance evaluation";
        ProcessInstance evaluation = startProcessFor(krisv, reason);

        TaskSummary startedTask = single(assignedTasksOf(krisv));
        assertThat(taskService.getTaskContent(startedTask.getId()), not(hasEntry("employee", krisv)));
        assertThat(taskService.getTaskContent(startedTask.getId()), hasEntry("reason", reason));
        assertThat(taskService.getTaskContent(startedTask.getId()), hasValue(krisv));
        assertThat(snapshotOf(startedTask), samePropertyValuesAs(selfEvaluationOf(evaluation)));
        assertThat(assignedTasksOf(john), is(empty()));
        assertThat(assignedTasksOf(mary), is(empty()));

        assertCompletingAssignedTaskMatching(selfEvaluationOf(evaluation), krisv, "exceeding");
        assertThat(snapshotOf(single(assignedTasksOf(john))), samePropertyValuesAs(projectManagerEvaluationOf(evaluation)));
        assertThat(snapshotOf(single(assignedTasksOf(mary))), samePropertyValuesAs(humanResourceEvaluationOf(evaluation)));

        assertCompletingAssignedTaskMatching(projectManagerEvaluationOf(evaluation), john, "acceptable");
        assertCompletingAssignedTaskMatching(humanResourceEvaluationOf(evaluation), mary, "outstanding");

        assertProcessInstanceNotActive(evaluation.getId(), session);
        assertProcessInstanceCompleted(evaluation.getId());
    }

    @Test
    public void checksProcessEventStack() {
        List<String> eventStack = new ArrayList<>();

        session.addEventListener(processEventsCollectorOf(eventStack));
        assertThat(eventStack, is(empty()));

        ProcessInstance pi = startProcessFor(krisv, "anything");
        //@formatter:off
        assertThat(eventStack, contains(
                "beforeVariableChanged:employee",
                "afterVariableChanged:employee",
                "beforeVariableChanged:reason",
                "afterVariableChanged:reason",
                "beforeProcessStarted:" + pi.getProcessId(),
                    "beforeNodeTriggered:",
                        "beforeNodeLeft:",
                            "beforeNodeTriggered:Self Evaluation",
                            "afterNodeTriggered:Self Evaluation",
                        "afterNodeLeft:",
                    "afterNodeTriggered:",
                "afterProcessStarted:" + pi.getProcessId()
        ));
        //@formatter:on

        eventStack.clear();

        Long taskId = single(assignedTasksOf(krisv)).getId();

        taskService.start(taskId, krisv);
        assertThat(eventStack, is(empty()));

        taskService.complete(taskId, krisv, ImmutableMap.of("performance", "exceeding"));
        //@formatter:off
        Matcher<Iterable<? extends String>> eventStackExpectation1 = contains(
                "beforeVariableChanged:performance",
                "afterVariableChanged:performance",
                "beforeNodeLeft:Self Evaluation",
                    "beforeNodeTriggered:",
                        "beforeNodeLeft:",
                            "beforeNodeTriggered:HR Evaluation",
                            "afterNodeTriggered:HR Evaluation",
                            "afterNodeLeft:",
                            "beforeNodeLeft:",
                            "beforeNodeTriggered:PM Evaluation",
                            "afterNodeTriggered:PM Evaluation",
                        "afterNodeLeft:",
                    "afterNodeTriggered:",
                "afterNodeLeft:Self Evaluation"
        );
        Matcher<Iterable<? extends String>> eventStackExpectation2 = contains(
                "beforeVariableChanged:performance",
                "afterVariableChanged:performance",
                "beforeNodeLeft:Self Evaluation",
                    "beforeNodeTriggered:",
                        "beforeNodeLeft:",
                            "beforeNodeTriggered:PM Evaluation",
                            "afterNodeTriggered:PM Evaluation",
                            "afterNodeLeft:",
                            "beforeNodeLeft:",
                            "beforeNodeTriggered:HR Evaluation",
                            "afterNodeTriggered:HR Evaluation",
                        "afterNodeLeft:",
                    "afterNodeTriggered:",
                "afterNodeLeft:Self Evaluation"
        );
        assertThat(eventStack, either(eventStackExpectation1).or(eventStackExpectation2));
        //@formatter:on
    }

    @Test
    public void writesAuditLog() throws IOException {
        File auditLogFile = new File("target/audit-log.log");
        auditLogFile.delete();

        KieRuntimeLogger auditLog = KieServices.get().getLoggers().newFileLogger(session, "target/audit-log");

        startProcessFor(krisv, "test");
        auditLog.close();

        assertTrue("audit log file is not created", auditLogFile.exists());
        assertThat(asCharSource(auditLogFile, Charset.defaultCharset()).read(), containsString("<processId>com.sample.evaluation</processId>"));
    }

    @Test
    public void abortStartedProcess() {
        ProcessInstance evaluation = startProcessFor(krisv, "anything");

        session.abortProcessInstance(evaluation.getId());

        assertProcessInstanceNotActive(evaluation.getId(), session);
        assertProcessInstanceAborted(evaluation.getId());
    }

    @Test
    public void startsProcessWithCorrelationKey() {
        CorrelationKey key = correlationKey("test");
        assertThat(allPersistedCorrelationKeys().size(), equalTo(0));

        ProcessInstance started = startProcessFor(krisv, "anything", key);
        assertThat(allPersistedCorrelationKeys().size(), equalTo(1));
        assertThat(single(allPersistedCorrelationKeys()).getName(), equalTo("test"));

        ProcessInstance result = getProcessInstanceBy(searchBasedCorrelaionKey("test"));
        assertThat("absent", result, is(notNullValue()));
        assertThat(result.getId(), equalTo(started.getId()));
    }

    @Test
    public void canNotLoadProcessInstanceThatStartByDuplicatedKeys() {
        ProcessInstance first = startProcessFor(krisv, "anything", correlationKey("duplicated"));
        ProcessInstance second = startProcessFor(krisv, "anything", correlationKey("duplicated"));
        assertThat(first.getId(), not(equalTo(second.getId())));
        assertThat(assignedTasksOf(krisv), hasSize(2));

        assertThat(getProcessInstanceBy(searchBasedCorrelaionKey("duplicated")), is(nullValue()));
    }

    private List<? extends CorrelationKey> allPersistedCorrelationKeys() {
        EntityManager em = getEmf().createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        Class<? extends CorrelationKey> entityClass = CorrelationKeyInfo.class;
        CriteriaQuery<? extends CorrelationKey> query = cb.createQuery(entityClass);
        query.from(entityClass);
        return em.createQuery(query).getResultList();
    }

    private ProcessEventListener processEventsCollectorOf(List<String> eventsCollector) {
        return ProcessEventListener.class.cast(Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), new Class[]{ProcessEventListener.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) {
                if (isEqualsMethod(method)) {
                    return proxy == args[0];
                }
                if (isEvent(method)) {
                    collectEvent(method.getName(), (ProcessEvent) args[0]);
                }
                return null;
            }

            private void collectEvent(String type, ProcessEvent event) {
                eventsCollector.add(type + ":" + extractNameFromEvent(event).trim());
            }

            private String extractNameFromEvent(ProcessEvent event) {
                if (event instanceof ProcessVariableChangedEvent) {
                    return ((ProcessVariableChangedEvent) event).getVariableId();
                }
                if (event instanceof ProcessNodeEvent) {
                    return ((ProcessNodeEvent) event).getNodeInstance().getNodeName();
                }
                return String.valueOf(event.getProcessInstance().getProcessId());
            }

            private boolean isEvent(Method method) {
                return method.getReturnType() == void.class && method.getParameterCount() == 1 && KieRuntimeEvent.class.isAssignableFrom(method.getParameterTypes()[0]);
            }

            private boolean isEqualsMethod(Method method) {
                return method.getName().equals("equals") && method.getParameterCount() == 1 && method.getReturnType() == boolean.class;
            }
        }));
    }

    public ProcessInstance getProcessInstanceBy(CorrelationKey key) {
        return ((CorrelationAwareProcessRuntime) session).getProcessInstance(key);
    }

    private ProcessInstance startProcessFor(String userId, String reason) {
        return startProcessFor(userId, reason, null);
    }

    public ProcessInstance startProcessFor(String userId, String reason, CorrelationKey key) {
        return EvaluationProcess.of(session).startProcessFor(userId, reason, key);
    }

    private TaskSummarySnapshotBuilder humanResourceEvaluationOf(ProcessInstance instance) {
        return as(instance).named("HR Evaluation").inState(Ready);
    }

    private TaskSummarySnapshotBuilder projectManagerEvaluationOf(ProcessInstance instance) {
        return as(instance).named("PM Evaluation").inState(Ready);
    }

    private TaskSummarySnapshotBuilder selfEvaluationOf(ProcessInstance instance) {
        return as(instance).named("Self Evaluation").describedAs("Please perform a self-evaluation.")
                .ownedBy(krisv).createdBy(krisv).inState(Reserved);
    }

    private void assertCompletingAssignedTaskMatching(TaskSummarySnapshotBuilder snapshot, String userId, String performance) {
        TaskSummary task = single(assignedTasksOf(userId));

        if (task.getStatus() == Ready) {
            taskService.claim(task.getId(), userId);
            assertThat(snapshotOf(single(assignedTasksOf(userId))), samePropertyValuesAs(snapshot.ownedBy(userId).inState(Reserved)));
        }

        taskService.start(task.getId(), userId);
        assertThat(snapshotOf(single(assignedTasksOf(userId))), samePropertyValuesAs(snapshot.ownedBy(userId).inState(InProgress)));

        taskService.complete(task.getId(), userId, ImmutableMap.of("performance", performance));
        assertThat(assignedTasksOf(userId), is(empty()));
    }

    private List<TaskSummary> assignedTasksOf(String userId) {
        return taskService.getTasksAssignedAsPotentialOwner(userId, "en_US");
    }

}
