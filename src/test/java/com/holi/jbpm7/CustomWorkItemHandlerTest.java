package com.holi.jbpm7;

import com.holi.jbpm7.mocks.CustomWorkItemHandler;
import org.jbpm.runtime.manager.impl.DefaultRegisterableItemsFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.kie.api.runtime.manager.*;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.internal.runtime.manager.context.EmptyContext;

import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class CustomWorkItemHandlerTest {

    @Test
    @Ignore("Can't discovery by JPA DeploymentDescriptor")
    public void checksCustomWorkItemHandlerUsed() {
        RegisterableItemsFactory itemsFactory = new DefaultRegisterableItemsFactory();
        RuntimeEnvironment environment = RuntimeEnvironmentBuilder.Factory.get().newDefaultInMemoryBuilder()
                .classLoader(getClass().getClassLoader())
                .registerableItemsFactory(itemsFactory).get();

        RuntimeManager runtimeManager = RuntimeManagerFactory.Factory.get().newSingletonRuntimeManager(environment);

        Map<String, WorkItemHandler> handlers = itemsFactory.getWorkItemHandlers(runtimeManager.getRuntimeEngine(EmptyContext.get()));

        assertThat(handlers, aMapWithSize(1));
        assertThat(handlers, hasEntry(equalTo("Log"), is(instanceOf(CustomWorkItemHandler.class))));
    }
}

