package com.holi.jbpm7.mocks;

import org.jbpm.process.instance.impl.demo.DoNothingWorkItemHandler;

public class CustomWorkItemHandler extends DoNothingWorkItemHandler {
    private static CustomWorkItemHandler instance = null;

    public CustomWorkItemHandler() {
        instance = this;
    }

    public static boolean isInUse() {
        return instance != null;
    }

    public static void reset() {
        instance = null;
    }
}
