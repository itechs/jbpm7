package com.holi.jbpm7.services;

import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;
import org.jbpm.kie.services.impl.bpmn2.BPMN2DataServiceImpl;
import org.jbpm.services.api.DefinitionService;
import org.jbpm.services.api.model.ProcessDefinition;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.google.common.io.Resources.getResource;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class DefinitionServiceTest {


    @Test
    public void extractProcessInformationFromProcessDefinition() throws IOException {
        DefinitionService definitionService = new BPMN2DataServiceImpl();

        ProcessDefinition definition = definitionService.buildProcessDefinition("test", Resources.toString(getResource("Evaluation.bpmn"), Charset.defaultCharset()), null, false);

        assertThat(definition.getDeploymentId(), equalTo(""));
        assertThat(definition.getPackageName(), equalTo("defaultPackage"));
        assertThat(definition.getVersion(), equalTo("1"));
        assertThat(definition.getId(), equalTo("com.sample.evaluation"));
        assertThat(definition.getName(), equalTo("Evaluation"));
        assertThat(definition.getType(), equalTo("RuleFlow"));
        assertThat(definition.getKnowledgeType(), equalTo("PROCESS"));
        assertThat(definition.getProcessVariables().keySet(), equalTo(ImmutableSet.of("employee", "reason", "performance")));
        assertThat(definition.getServiceTasks(), anEmptyMap());
        assertThat(definition.getAssociatedEntities().keySet(), equalTo(ImmutableSet.of("Self Evaluation", "HR Evaluation", "PM Evaluation")));
        assertThat(definition.getGlobals(), is(empty()));
        assertThat(definition.getReferencedRules(), is(empty()));
        assertThat(definition.getReusableSubProcesses(), is(empty()));
    }
}
