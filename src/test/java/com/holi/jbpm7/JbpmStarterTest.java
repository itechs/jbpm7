package com.holi.jbpm7;

import org.drools.core.impl.KnowledgeBaseFactory;
import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.kie.internal.process.CorrelationAwareProcessRuntime;
import org.kie.internal.utils.KieHelper;

import java.util.List;
import java.util.function.Supplier;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class JbpmStarterTest extends JbpmJUnitBaseTestCase {
    private Supplier<KieBase> knowledgeFactory;

    public JbpmStarterTest(Supplier<KieBase> knowledgeFactory) {
        this.knowledgeFactory = knowledgeFactory;
    }

    @Parameters
    public static List<Supplier<KieBase>> factories() {
        return asList(
                new KieHelper()::build,
                KnowledgeBaseFactory::newKnowledgeBase
        );
    }

    @Test
    public void createSessionFromEmptyKnowledgeBase() {
        KieBase knowledge = knowledgeFactory.get();
        KieSession session = knowledge.newKieSession();

        assertThat(knowledge.getEntryPointIds(), is(empty()));
        assertThat(knowledge.getProcesses(), is(empty()));
        assertThat(session.getEntryPointId(), equalTo("DEFAULT"));
    }

    @Test
    public void supportsCorrelationKeys() {
        assertThat(knowledgeFactory.get().newKieSession(), is(instanceOf(CorrelationAwareProcessRuntime.class)));
    }
}
