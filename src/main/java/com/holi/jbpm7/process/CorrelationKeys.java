package com.holi.jbpm7.process;

import org.jbpm.persistence.correlation.CorrelationKeyInfo;
import org.jbpm.persistence.correlation.CorrelationPropertyInfo;
import org.kie.internal.process.CorrelationKey;

public class CorrelationKeys {

    public static CorrelationKey searchBasedCorrelaionKey(String name) {
        CorrelationKeyInfo key = new CorrelationKeyInfo();
        key.addProperty(propertyWithValueOnly(name));
        return key;
    }

    public static CorrelationKey correlationKey(String name) {
        CorrelationKeyInfo key = new CorrelationKeyInfo();
        key.setName(name);
        key.addProperty(justToBeSerialized());
        return key;
    }

    private static CorrelationPropertyInfo propertyWithValueOnly(String value) {
        return new CorrelationPropertyInfo(null, value);
    }

    private static CorrelationPropertyInfo justToBeSerialized() {
        return propertyWithValueOnly("<unused>");
    }
}
