package com.holi.jbpm7.task;

import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.TaskSummary;

public interface TaskSummarySnapshot {
    String getProcessId();

    Long getProcessInstanceId();

    String getName();

    String getSubject();

    String getDescription();

    String getCreatedById();

    String getActualOwnerId();

    Status getStatus();

    static TaskSummarySnapshot snapshotOf(TaskSummary summary) {
        return TaskSummarySnapshotRuntimeBuilder.from(summary)
                .named(summary.getName()).subject(summary.getSubject()).describedAs(summary.getDescription())
                .ownedBy(summary.getActualOwnerId())
                .createdBy(summary.getCreatedById())
                .inState(summary.getStatus());
    }

    static TaskSummarySnapshotBuilder as(ProcessInstance instance) {
        return TaskSummarySnapshotRuntimeBuilder.from(instance);
    }

    interface TaskSummarySnapshotBuilder extends TaskSummarySnapshot {
        TaskSummarySnapshotBuilder named(String name);

        TaskSummarySnapshotBuilder subject(String subject);

        TaskSummarySnapshotBuilder describedAs(String description);

        TaskSummarySnapshotBuilder createdBy(String userId);

        TaskSummarySnapshotBuilder ownedBy(String userId);

        TaskSummarySnapshotBuilder inState(Status status);
    }
}

class TaskSummarySnapshotRuntimeBuilder implements TaskSummarySnapshot.TaskSummarySnapshotBuilder {
    private final String processId;
    private final long processInstanceId;
    private Status status;
    private String ownerId;
    private String creatorId;
    private String description;
    private String subject;
    private String name;

    public TaskSummarySnapshotRuntimeBuilder(String processId, long processInstanceId) {
        this.processId = processId;
        this.processInstanceId = processInstanceId;
    }

    public static TaskSummarySnapshotRuntimeBuilder from(ProcessInstance instance) {
        return new TaskSummarySnapshotRuntimeBuilder(instance.getProcessId(), instance.getId());
    }

    public static TaskSummarySnapshotRuntimeBuilder from(TaskSummary summary) {
        return new TaskSummarySnapshotRuntimeBuilder(summary.getProcessId(), summary.getProcessInstanceId());
    }

    private TaskSummarySnapshotRuntimeBuilder copy() {
        return new TaskSummarySnapshotRuntimeBuilder(processId, processInstanceId).withName(name).withCreatorId(creatorId).withOwnerId(ownerId).withStatus(status);
    }

    @Override
    public TaskSummarySnapshotBuilder named(String name) {
        return copy().withName(name).withSubject(subject).withDescription(description);
    }

    private TaskSummarySnapshotRuntimeBuilder withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public TaskSummarySnapshotBuilder subject(String subject) {
        return copy().withSubject(subject);
    }

    private TaskSummarySnapshotRuntimeBuilder withSubject(String subject) {
        this.subject = subject;
        return this;
    }

    @Override
    public TaskSummarySnapshotBuilder describedAs(String description) {
        return copy().withDescription(description);
    }

    private TaskSummarySnapshotRuntimeBuilder withDescription(String description) {
        if (subject == null) {
            subject = description;
        }
        this.description = description;
        return this;
    }

    @Override
    public TaskSummarySnapshotBuilder createdBy(String userId) {
        return copy().withCreatorId(userId);
    }

    private TaskSummarySnapshotRuntimeBuilder withCreatorId(String userId) {
        creatorId = userId;
        return this;
    }

    @Override
    public TaskSummarySnapshotBuilder ownedBy(String userId) {
        return copy().withOwnerId(userId);
    }

    private TaskSummarySnapshotRuntimeBuilder withOwnerId(String userId) {
        ownerId = userId;
        return this;
    }

    @Override
    public TaskSummarySnapshotBuilder inState(Status status) {
        return copy().withStatus(status);
    }

    private TaskSummarySnapshotRuntimeBuilder withStatus(Status status) {
        this.status = status;
        return this;
    }

    @Override
    public String getProcessId() {
        return processId;
    }

    @Override
    public Long getProcessInstanceId() {
        return processInstanceId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getCreatedById() {
        return creatorId;
    }

    @Override
    public String getActualOwnerId() {
        return ownerId;
    }

    @Override
    public Status getStatus() {
        return status;
    }
}
