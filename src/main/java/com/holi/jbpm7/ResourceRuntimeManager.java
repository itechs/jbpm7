package com.holi.jbpm7;

import org.kie.api.runtime.manager.Context;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;

import java.util.Objects;

public interface ResourceRuntimeManager extends RuntimeManager, AutoCloseable {

    static ResourceRuntimeManager of(RuntimeManager origin) {
        Objects.requireNonNull(origin);
        return new ResourceRuntimeManager() {
            @Override
            public RuntimeEngine getRuntimeEngine(Context<?> context) {
                return origin.getRuntimeEngine(context);
            }

            @Override
            public String getIdentifier() {
                return origin.getIdentifier();
            }

            @Override
            public void disposeRuntimeEngine(RuntimeEngine runtime) {
                origin.disposeRuntimeEngine(runtime);
            }

            @Override
            public void close() {
                origin.close();
            }

            @Override
            public void signalEvent(String type, Object event) {
                origin.signalEvent(type, event);
            }
        };
    }
}
