# jBPM

jBPM是一个灵活的、轻量级的业务流程管理(BPM)组件。它为业务分析人员与开发人员建立起沟通的桥梁。然而传统的BPM引擎仅仅局限于技术人员。jBPM提供了双重目标：**它为业务用户以及开发人员提供了流程管理的统一方式**。


## jBPM用来做什么？ (What does jBPM do?)

一个业务流程允许你通过流程图将业务目标描述为一系列可执行的有序步骤来进行建模。这极大的提高了业务逻辑的可见性以及敏捷性，方便业务用户更容易理解及监控高层和特定领域的流程表现形式。

jBPM的核心库采是纯Java编写的一个轻量级的，可扩展的工作流引擎，允许你使用最新的BPMN 2.0规范来执行业务流程。它能直接嵌入到应用程序或以服务的方式运行于任何JVM环境中。

为了支持整个生命周期的业务流程，在核心引擎之上提供了许多特性和工具:

- 基于Eclipse/Web编辑器支持以图形的方式创建业务流程。
- 基于JPA/JTA可插拔的持久化以及分布式事物管理。
- 基于WS-HumanTask的可插入的人工任务服务，用于需要由人工参与的任务。
- 管理控制台支持流程实例管理，任务列表和任务表单管理以及生成报告。
- 能够将业务流程部署到可选的流程仓库。
- 能够查询/监控/分析历史日志。
- 能够与Seam，Spring，OSGi等第三方框架或组件进行集成。

能够将特定领域的流程节点插入到面板中，以便业务用户能够更容易的理解流程。

jBPM提供了适应的及动态的流程来构建真实情况复杂的不能用简单的过程来描述的流程提供了灵活性。我们通过允许他们控制流程的哪些部分应该执行，动态的偏离流程，等等将控制权交给终端用户。

jBPM不仅仅是一个独立的流程引擎。复杂的业务逻辑能够被建模成业务流程、业务规则和复杂事件处理的组合。jBPM能够与Drools结合使用以支持将业务逻辑建模为一组业务流程，规则以及件的范例集成到一个统一的环境。

## Maven依赖管理 (Maven Dependencies Management)

如果你使用Maven来管理项目的依赖，仅仅需要添加一个依赖到`pom.xml`中：

```
<dependency>
  <groupId>org.jbpm</groupId>
  <artifactId>jbpm-test</artifactId>
  <version>7.6.0.Final</version>
</dependency>
```

## 核心引擎API

### 概览 (Overview)

为了能够与流程引擎通信，如: 启动一个流程，你需要创建一个session.这个session将被用于于流程引擎进行通信。然而一个session需要引用一个包含所有相关流程定义的知识库(Knowledge Base)对象。这个Knowledge被用来在必要的时候查询流程定义(Process Definition)。在创建session对象的实例之前，首先需要创建一个Knowledge对象来装载所有所需的流程定义。

一旦session被创建，你能够使用它来执行一个流程。每当一个process被启动，一个新的ProcessInstance将被创建用来保存具体流程实例的状态。

![session setup](etc/KnowledgeBaseAndSession.png)

例如，假设你正在编写一个应用程序来处理销售订单。你可以通过一个或多个流程定义来解决订单如何被处理。当启动应用程序，你首先需要创建一个装载这些流程定义的Knowledge对象，然后可以通过Knowledge来创建一个session，每当一个新的销售订单产生，一个新的流程被启动。启动的流程实例包含具体销售请求流程的状态。

一个Knowledge能够跨session共享，通常在应用程序启动时创建一次，因为Knowledge是重量级的组建，它包涉及分析和编译流程定义。你可以在运行时期添加和删除流程。

Session可以基于Knowledge对象来创建用于执行流程以及与引擎交互。在需要时你可以创建互相独立的session，创建一个session被考虑为相对轻量级的。创建多少个session取决于你，可以使用session连接池来实现session的共享。通常大部分简单的情况创建一个session用于多个地方进行调用。你能够决定创建多个session来处理多个不相关的流程单元(Process Units)。例如，你希望来自一个用户的所有流程与其他用户的流程相互独立，你能够为每个用户创建一个独立的session或者你可能需要多个session实例来提升伸缩性。如果你不知道该怎么做，简单的创建一个包含所有流程定义的Knowledge并创建一个session来执行所有流程。

jBPM项目在用户(developer)与之交互的API和实际的实现类(internal api)之间有明确的分离。公开的API暴露了大部分特性，我们相信大部分用户能够安全的使用且在发布时保持相当稳定。专业人士仍然可以访问内部的实现类，但应该知道它们用来做什么且内部API可能在将来会发生变化。

如上所述，jBPM API因此应该被用于(1)：创建一个包含所有流程定义的Knowledge。以及(2): 创建一个session来启动一个新的ProcessInstance，发出现有实例的信号，注册监听器等等。


### 知识库 (KieBase)

jBPM API允许你首先创建一个KnowledgeBase。这个KnowledgeBase应该导入session需要执行的所有流程定义。使用一个KieHelper从多种资源加载流程来创建一个KnowledgeBase。下面的代码片段展示了如何创建一个KnowledgeBase只包含一个流程定义(这个例子中资源从类路径中加载)。

```
KieHelper kieHelper = new KieHelper();
KieBase kieBase = kieHelper
                  .addResource(ResourceFactory.newClassPathResource("MyProcess.bpmn"))
                  .build();
```

`ResourceFactory`有类似的方法从文件系统，`URL`，`InputStream`，`Reader`等中夹在文件。

这被认为是手动创建KnowledgeBase，虽然他很简单，但不推荐用于真正的应用程序开发，而更多用于试用。接下来你会发现使用`RuntimeManager`创建知识库，知识会话等更强大的方法。


### Kie会话 (KieSession)

一旦你加载完KnowledgeBase，你应该创建一个session来与引擎交互。这个session能够被用来启动一个新的流程，发送信号事件，等等。下面的代码片段显示如何简单的使用之前创建的KnowledgeBase来创建一个session启动一个流程。


```
KieSession ksession = kbase.newKieSession();
ProcessInstance processInstance = ksession.startProcess("com.sample.MyProcess");
```


### 流程实例管理对象 (ProcessRuntime)

`ProcessRuntime`接口定义了session与流程交互的所有方法。如启动流程、终止流程、向引擎发送事件，等等。

### 事件监听器 (Event Listeners)

Session提供了注册和移除事件监听器的方法。一个`ProcessEventListener`能被用于监听流程的相关事件，如启动或完成一个流程，进入和退出一个节点，等事件。

关于事件**之前**和**之后**说明：这些事件通常就像一个调用堆栈，也就是说之前事件的结果作为任何事件的产生。例如，如果后续的节点的触发为离开节点的结果，节点触发的事件将产生在剩下的节点的`beforeNodeLeftEvent`和`afterNodeLeftEvent`之间(如第二个节点的触发是第一个节点退出事件的结果)。这样做可以让我们更容易的导出事件之间的原因关系。类似地，所有节点的触发事件和节点退出事件将作为流程事件的直接结果。通常如果你只是想在特定事件发生时得到通知，那么你应该只需查看before事件，当只查看after事件，可能会得到事件以错误的触发顺序，这是因为after事件被触发成堆栈。after事件只有在所有事件被触发后才会被触发。after事件应该被用于处理所有相关事件已结束的情况。如，你想在启动一个特定的流程后得到通知。

另外需要注意的地方是并非所有节点都始终生成节点触发事件(trigger events)和节点退出事件(left events)。根据节点的类型，一些节点可能只生成节点退出事件，其他节点可能只生成节点触发事件。例如，捕捉中间事件(Catching intermediate events)不会产生触发事件, 它们只生成退出事件，因为它们并非被其他节点触发，而是从外部激活。类似地，抛出中间事件(throwing intermediate events)不生成退出事件，它们只生成触发事件因为他们不是真正的退出，它们没有传出链接。

jBPM提供了一个开箱即用的审计日志监听器(以控制台方式或文件方式)。审计日志包含所有在运行时产生的不同事件，因此很容易找出发生的事情。注意，*这些日志对象只能用于调试*。支持的日志实现如下：

- Console logger： 将所有事件打印到控制台上。
- File logger： 将所有事件以XML格式写入到一个文件中。该日志文件可能被IDE用于以树结构形式显示执行期间的所有相关事件。
- Thread file logger：由于一个文件式的logger只在关闭时或事件数量达到预定的级别时才写入磁盘，他不能在运行期被用于调试流程(processes)。一个线程文件日志将在特定的时间段将所有日志写入文件中。可以在调试阶段使用记录器实时查看流程进度。

如下所示，`KieServices`能够让你向session中添加一个`KieRuntimeLogger`。当创建一个控制台日志记录器需要传入session的实例。文件记录器还需要一个日志文件名称，线程文件记录器需要传递一个保存事件的时间间隔(以毫秒为单位)。

```
import org.kie.api.KieServices;
import org.kie.api.logger.KieRuntimeLogger;
...
KieRuntimeLogger logger = KieServices.Factory.get().getLoggers().newFileLogger(ksession, "test");
// add invocations to the process engine here,
// e.g. ksession.startProcess(processId);
...
logger.close();
```

基于文件的日志将创建一个包含所有在运行时发生的事件的XML格式的日志文件。它能够使用Drools Eclipse插件中的Audit View打开，事件将以树结构形式显示。发生在before和after事件之间的事件被显示为该事件的子节点。下面的截图显示了一个简单的例子，当流程被启动，导致一个Start节点，一个Action节点和一个End节点的激活，之后流程被完成。

![audit view](etc/AuditView.png)


### 相关键 (Correlation Keys)

处理流程时的一个常见需求是能够为给定流程实例分配某种业务标识(Business Identifier)以便能够在稍后不知道流程实例ID的情况下进行引用。

为提供该功能，jBPM允许使用包含`CorrelationProperties`的`CorrelationKey`。`CorrelationKey`能够包含一个或者多个属性来描述流程。

相关性(Correlation)功能由接口`CorrelationAwareProcessRuntime`的一部分提供。

相关性通常用于长时间运行的流程且需要启用持久化来保存相关性的信息。


### 线程 (Threads)

在下文中，我们将引入两种类型的“多线程”：逻辑型(logical)和技术型(technical)。技术型多线程是在计算机上启动多个线程或进程是发生的情况，如Java或C程序。逻辑多线程是我们在业务流程管理(BPM)流程中的某个流程抵达一个并行网关(parallel gateway)后，例如从功能角度上来看，原来的流程将分裂成两个并行执行的流程。

当然，jBPM引擎支持逻辑型多线程：如，包含一个并行网关的流程。jBPM使用一个线程来实现逻辑型多线程：**一个包含逻辑性多线程的jBPM流程只能在一个技术型线程中执行。**这样做的主要原因是，如果多个技术型线程正在处理相同的流程，则需要能够彼此通信已达到流程状态共享。这个需求带来了一些复杂性。虽然看起来多线程能够带来性能的优势，但多个线程协同工作的额外的逻辑并不能保证性能能够有所提升。而且使用技术型多线程来实现逻辑型多线程还有额外的开销，因为我们需要避免多线程中的竞争条件(race conditions)和死锁(dead lock)等问题。

通常，jBPM引擎以串行(in serial)的方式执行操作(actions)。例如，当引擎在流程中遇到一个脚本任务(Script Task)时，它将同步执行该脚本(script)并等待脚本完成后继续执行。类似的，如果一个流程遇到一个并行网关，它将一个接一个一次触发每个传出分支(outgoing branches)。这是可能的，因为执行几乎总是即时的，这意味着它非常快且几乎不会产生额外的开销。结果，用户通常甚至不会注意到这一点。同样，流程中的动作脚本(action scripts)也会同步执行，并且引擎在继续流程之前等待它们完成。流入，将`Thread.sleep(...)`作为脚本的一部分不会使引擎在其他地方继续执行，但会在此期间阻塞引擎线程。

同样的原则适应与服务任务(service task)。当一个流程抵达服务任务时，引擎将同步(synchronously)执行该服务的处理器(handler)。引擎将等待`completeWorkItem(...)`方法返回才继续执行。如果不需要即时执行一个服务任务，那么在同步触发服务任务之后异步执行服务非常重要，可以采用消息队列来执行异步的服务任务。


一个例子就是调用外部服务(external service)的服务任务。由于调用外部服务有延迟并等待其结果可能太长，因此异步调用服务可能是个好的注意。这以为这处理程序只会调用服务，并在结果可用时稍后通知引擎。同时，流程引擎继续执行后续流程。

人工任务(Human tasks)是需要异步调用的服务的典型例子，因为我们不希望引擎等待任务参与者响应请求。当人工任务节点被触发时，人工任务处理器将创建一个新的任务(在所分配的任务的任务列表上)。然后引擎将能够继续执行剩下的流程(如果必要)，并且任务处理器将在用户完成任务时异步通知引擎。


## 运行时管理器 (RuntimeManager)

### 概览 (Overview)

引入`RuntimeManager`来简化和强化知识API的使用，尤其在流程上下文中。它提供了可配置的策略(configurable strategies)来控制实际运行时的执行，默认情况下提供了以下内容：

- Singleton：无论流程数量如何，运行时管理器都只保持单个`KieSession`的实例
- Per Request：运行时管理器为每个请求提供一个新的`KieSession`实例
- Per Process Instance：运行时管理器维护流程实例和`KieSession`之间的映射，并且每当使用给定的流程实例总是提供相同的`KieSession`。


运行时管理器的主要负责管理`RuntimeEngine`实例并将其传递给调用者。反过来，`RuntimeEngine`封装了jBPM引擎的两个重要元素：

- KieSession
- TaskService

这两个组建都已经配置为可以顺利的彼此协作而无需最终用户使用额外的配置。不需要注册人工任务处理程序(human task handler)或跟踪是是否连接到服务(service)。


`RuntimeManager`将确保无论采用的策略如何，在`RuntimeEngine`的初始化和配置时都会提供相同的功能。这意味着：

- `KieSession`将被加载相同的工厂(无论是内存还是基于JPA)
- `WorkItemHandlers`将被注册到每个`KieSession`中(从数据库夹在或新建)
- 事件监听器(Process, Agenda, WorkingMemory)将在每个`KieSession`中注册(从数据库中加载或新建)
- `TaskService`将被配置为：
  - JTA分布式事务管理器
  - `KieSession`具有相同的实体管理器工厂(EntityManagerFactory)
  - 来自环境的`UserGroupCallback`

另一方面，当`RuntimeEngine`不再需要释放可能获得的资源，`RuntimeManager`通过提供专用的方法来处理`RuntimeEngine`对使用资源的释放。


> `Runtime`的标识符(identifier)在运行期间用作`deploymentId`。例如，标识符在任务持久化时保存为`deploymentId`。`Task`的`deploymentId`用于任务完成和其流程实例恢复时关联`RuntimeManager`。`deploymentId`也作为历史日志表(history log tables)中的`externalId`存储。如果你在`RuntimeManager`创建时未指定标识符，则会应用默认值，如：`PerProcessInstanceRuntimeManager`的`"default-per-instance"`。这意味着你的应用程序在其生命周期中使用相同的部署。如果在应用程序中维护多个`RuntimeManager`s则需要指定其标识符。例如，jbpm-services中的(`DeploymentService`)维护具有kjar的GAV标识的多个`RuntimeManager`s。kie-workbench的Web应用程序管理`RuntimeManager`s取决与jbpm-services。


### 策略 (Strategies)

单例策略(Singleton Strategy) - 指`RuntimeManager`维护`RuntimeEngine`的单个实例(以及`KieSession`和`TaskSession`的单个实例)。对于`RuntimeEngine`的访问是同步的，并且是线程安全的，但是由于同步机制会带来一定的性能损失。该策略类似于jBPM 5.x中默认提供的策略，由于它是最简单的策略所以建议开始时使用单例策略。

它具有以下的特性，对于评估给定场景考虑时非常重要。

- 占用内存低(footprint) - 单一的运行时引擎和任务服务实例
- 简单紧凑的设计和使用
- 由于同步访问，非常适合流程引擎低中等负载
- 由于单个`KieSession`实例，所有状态对象(如事实facts)可以直接对有所有流程实例可见，反之亦然。
- 上下文无关(not contextual) - 意味着从单一的`RuntimeManager`上下文中获取`RuntimeEngine`实例并不重要，通常使用`EmptyContext.get()`，尽管可以使用`null`参数。
- 跟踪在`RuntimeManager`重启之间使用的`KieSession`的`ID`，以确保它将使用相同的会话，取决于环境可以是以下之一：
  - 由`jbpm.data.dir`系统属性给出的值
  - 由`jbpm.server.data.dir`系统属性给出的值
  - 由`java.io.tmpdir`系统属性给出的值


按请求策略(Per Request Strategy) - 指`RuntimeManager`为每个请求提供一个新的`RuntimeEngine`实例。由于请求`RuntimeManager`将考虑单个事务中的一个或多个调用。他必须在单个事务中返回相同的`RuntimeEngine`实例，以确保状态的正确性，否则在一次调用中完成的操作将不会在另一次调用中可见。这是一种“无状态(stateless)”策略，只提供请求范围内的状态，一旦完成请求，`RuntimeEngine`将被永久销毁 - 如果使用持久化, `KieSession`中的信息将从数据库中删除。

它有如下的特性：

- 为每个请求提供完全独立的流程引擎和任务操作
- 完全无状态，只在请求期间保存`facts`实例
- 支持高负载，无状态的进程(请求之间没有涉及`facts`和定时器`timers`)
- `KieSession`只在请求的生命周期内可用，并在请求结束后销毁
- 上下文无关(not contextual) - 意味着从每个请求中获取`RuntimeEngine`的实例`RuntimeManager`上下文实例并不重要，通常使用`EmptyContext.get()`，尽管可以使用`null`作为参数。


按流程实例策略(Per process instance strategy) - 指`RuntimeManager`维护`KieSession`和`ProcessInstance`之间的严格关系。这意味着只要它所属的`ProcessInstance`处于活动(active)状态，`KieSession`就可用。该策略提供了最灵活的方法来使用引擎的高级特性如独立的规则计算(rule evaluation in isolation)，但仅针对给定的流程实例，最大化性能并减少同步引起的潜在瓶颈。同时将`KieSession`的数量减少到实际的流程实例数量，而不是请求数量(与每个请求策略相反)。

它具有如下的特性：

- 只为给定的流程实例提供了最先进的隔离策略。
- 在`KieSession`和`ProcessInstance`之间保持严格(strict)的关系，以确保它始终为给定的`ProcessInstance`提供相同的`KieSession`。
- 将`KieSession`的生命周期与`ProcessInstance`合并，使得两者都可以在流程实例完成时得以释放(完成或终止)
- 允许维护流程实例范围内的数据(如：事实fact,定时器timer) - 只有流程实例才能访问该数据。
- 由于需要查找并加载给定流程实例的`KieSession`所以引入了额外的开销。
- 验证`KieSession`的使用情况，以便它不能被用于或滥用于其他流程实例，在这种情况下跑出异常。
- 上下文相关(is contextual) - 允许如下的上下文(`Context<?>`)实例：
  -  `EmptyContext`或`null` - 当流程启动时因为没有可用的流程实例表示(ID)
  - `ProcessInstanceIdContext` - 在流程实例被创建后使用
  - `CorrleationKeyContext` - 用作`ProcessInstanceIdContext`的替代方法以使用自定义业务键(business key)而不是流程ID

### 运行时期管理器的使用 (Usage)

`RuntimeManager`的常规使用场景是：

- 在应用程序启动时 (At application startup)
  - 构建`RuntimeManager`并在应用程序的整个生命周期中保持它，他是线程安全的并且可以(甚至应该)同时访问

- 在请求时 (At request)
  - 使用适当的上下文实例从`RuntimeManager`中获取`RuntimeEngine`的实例。
  - 从`RuntimeEngine`中获取`KieSession`和`TaskService`
  - 在`KieSession`或`TaskService`上执行例如`startProcess`，`completeTask`等操作
  - 一旦处理完成，使用`RuntimeManager.disposeRuntimeEngine`方法来释放`RuntimeEngine`。

- 在应用程序关闭时 (At application shutdown)
  - 关闭`RuntimeManager`

> 当`RuntimeEngine`从一个活动的**active** JTA事务中的`RuntimeManager`获得，则不需要在最后释放`RuntimeEngine`。因为`RuntimeManager`会在事务完成时自动销毁`RuntimeEngine`(不管完成状态是提交还是回滚）


### 例子 (Example)

以下是如何构建`RuntimeManager`并从中获取`RuntimeEngine`(封装了`KieSession`和`TaskService`)的方法：

```
// first configure environment that will be used by RuntimeManager
RuntimeEnvironment environment = RuntimeEnvironmentBuilder.Factory.get()
      .newDefaultInMemoryBuilder()
      .addAsset(ResourceFactory.newClassPathResource("BPMN2-ScriptTask.bpmn2"), ResourceType.BPMN2)
      .get();

// next create RuntimeManager - in this case singleton strategy is chosen
RuntimeManager manager = RuntimeManagerFactory.Factory.get().newSingletonRuntimeManager(environment);

// then get RuntimeEngine out of manager - using empty context as singleton does not keep track
// of runtime engine as there is only one
RuntimeEngine runtime = manager.getRuntimeEngine(EmptyContext.get());

// get KieSession from runtime runtimeEngine - already initialized with all handlers, listeners, etc that were configured
// on the environment
KieSession ksession = runtimeEngine.getKieSession();

// add invocations to the process engine here,
// e.g. ksession.startProcess(processId);

// and last dispose the runtime engine
manager.disposeRuntimeEngine(runtimeEngine);
```

这个例子提供了使用`RuntimeManager`和`RuntimeEngine`的最简单(最小)的方式，尽管它提供了一些非常有价值的信息：

- `KieSession`将只在内存中 - 通过使用`newDefaultInMemoryBuilder`
- 有唯一一个流程可以被执行 - 将其作为资产`asset`添加
- `TaskService`将通过`LocalHTWorkItemHandler`进行配置并附加到`KieSession`以支持流程内的用户任务功能。


### 配置 (Configuration)

知道如何创建，销毁，注册处理程序等复杂性被从最终用户转移到运行时管理器中，运行时起流程管理器知道何时/如何执行这些操作，但仍然允许通过提供运行时环境的全面配置来对流程进行细粒度(fine grained)的控制。


### 构建运行时环境 (Build RuntimeEnvironment)

虽然`RuntimeEnvironment`接口主要提供对作为环境一部分的数据访问，并将被`RuntimeManager`使用。用户应该使用构造器(builder)样式提供流程的API来配置`RuntimeEnvironment`的预定义设置。

`RuntimeEnvironmentBuilder`的实例可以通过`RuntimeEnvironmentBuilderFactory`获得，`RuntimeEnvironmentBuilderFactory`提供了预配置的一组构造器，以简化并帮助用户构建`RuntimeManager`的环境。

出`KieSession`运行时管理器之外，还提供对`TaskService`的访问权限，作为`RuntimeEngine`的集成组建，它将始终配置并准备好用于流程引擎和任务服务中。

由于使用了默认的构建器，因此它将包含预定义的一组元素：

- 持久化单元(`PeristenceUnit`)的名称将被设置为`org.jbpm.persistence.jpa`(用于流程引擎和任务服务中)
- 人工任务处理器(Human Task Handler)将被自动注册到`KieSession`中
- 基于JPA的历史日志事件监听器将被自动注册到`KieSession`中
- 触发规则任务计算(rule task evaluation)`fireAllRules`的事件监听器(Event listener)将被自动注册到`KieSession`上

### 注册处理器和监听器 (Register handlers and listeners)

提供了一个专用的机制, 作为`RegisterableItemsFactory`的实现来使用自己的处理器或监听器来扩展它。

最佳实践为仅仅扩展那些开箱即用的实现并添加你自己的东西。因为`RegisterableItemsFactory`的默认实现提供了定义自定义处理器和监听器的可能性，所以并不总是需要扩展。以下列出了可能有用的实现（他们按继承层次结构排序）：

- `org.jbpm.runtime.manager.impl.SimpleRegisterableItemsFactory` - 最简单的可用的实现，并且基于反射来根据给定处理器或监听器的类名来创建相应的实例。
- `org.jbpm.runtime.manager.impl.DefaultRegisterableItemsFactory` - 是简单实现的超级，它包含了上述的默认设置并提供了与简单实现相同的功能。
- `org.jbpm.runtime.manager.impl.KModuleRegisterableItemsFactory` - 为默认实现提供了`kmodule`特定功能的扩展，并提供与简单实现相同的功能。
- `org.jbpm.runtime.manager.impl.cdi.InjectableRegisterableItemsFactory` - 为**CDI**环境定制了默认实现的扩展，并提供了CDI风格的方法通过`@Produce`s来查找处理器和监听器。

另外，简单的(无状态的或仅需要`KieSession`的)工作项处理器(work item handlers)可以以众所周知的方式注册 - 定义为`CustomWorkItem.conf`文件的一部分，该文件放置在类路径上。要使用此方法，请执行以下操作：

- 在`META-INF`下创建一个`drools.session.conf`文件，Web应用程序为`WEB-INF/classes/META-INF`.
- 添加如下行到`drools.session.conf`文件中`drools.workItemHandlers = CustomWorkItemHandlers.conf`
- 在`META-INF`下创建一个`CustomWorkItemHandlers.conf`文件，Web应用程序的路径为`WEB-INF/classes/META-INF`
- 在`CustomWorkItemHandlers.conf`中使用MVEL风格定义自定义的工作项处理器

```
[
  "Log": new org.jbpm.process.instance.impl.demo.SystemOutWorkItemHandler(),
  "WebService": new org.jbpm.process.workitem.webservice.WebServiceWorkItemHandler(ksession),
  "Rest": new org.jbpm.process.workitem.rest.RESTWorkItemHandler(),
  "Service Task" : new org.jbpm.process.workitem.bpmn2.ServiceTaskHandler(ksession)
]
```

就这样，现在所有这些工作项处理器都将被注册为由该程序创建的任何`KieSession`上，无论它是否使用`RuntimeManager`。


### 服务 (Services)

从jBPM6.2开始在`RuntimeManager` API之上提供了一组高级别的服务。这些服务能够以最简单的方式将jBPM功能嵌入到自定义的应用程序中。一个完整模块作为这些服务的一部分提供。他们被分成几个模块，以便在各种环境中简化使用。


- **jbpm-services-api** 只包含API相关的类和接口
- **jbpm-kie-services** 重写服务api的代码实现 - 纯Java，没有框架依赖
- **jbpm-services-cdi** 在核心的服务实现之上提供了CDI的包装
- **jbpm-services-ejb-api** 为EJB提供了服务api的扩展
- **jbpm-services-ejb-timer** 一个基于EJB `TimerService`的调度服务支持基于时间的操作，如： 计时器事件(timer events)、截止日期(deadlines)，等。
- **jbpm-services-ejb-client** EJB远程客户端实现 - 目前仅用于JBoss服务模块按其框架依赖关系进行分组，因此开发人员可自由选择哪一种适合他们并仅使用它。


### 部署服务 (Deployment Service)

正如其名，其主要职责是部署和取消部署单元(DeploymentUnit).部署单元是用于执行业务资产(如流程，规则，表单，数据模型)的kjar。部署服务允许查询它以获取可用的部署单元，甚至它们的`RuntimeManager`实例。

>在EJB远程客户端上有一些限制，不要暴露`RuntimeManager`因为它在客户端序列化后没有任何意义。

因此，此服务的典型用例是为你的系统提供动态行为，以便多个kjars可以同时处于活动状态并同时执行。

```
// create deployment unit by giving GAV
DeploymentUnit deploymentUnit = new KModuleDeploymentUnit(GROUP_ID, ARTIFACT_ID, VERSION);
// deploy
deploymentService.deploy(deploymentUnit);
// retrieve deployed unit
DeployedUnit deployed = deploymentService.getDeployedUnit(deploymentUnit.getIdentifier());
// get runtime manager
RuntimeManager manager = deployed.getRuntimeManager();
```


### 定义服务 (Definition Service)

部署完成后，每个流程定义(process definition)都将使用定义服务进行扫描，定义服务可以分析流程并从中提取有价值的信息。这些信息可以为系统提供有价值的信息，告知用户预期的内容。定义服务提供有关一下的信息：

- 流程的定义 - id, 名称(name), 描述(description)
- 流程变量 - 变量名称和类型(type)
- 流程中重复使用的子流程(如果有的话)
- 服务任务(领域特定的活动)
- 用户任务(user task)包含分配信息
- 任务数据(task data)输入和输出信息


因此定义服务可以被看成是一种支持服务，它提供了很多关于直接从BPMN2中提取的流程定义信息。

```
String processId = "org.jbpm.writedocument";

Collection<UserTaskDefinition> processTasks =
bpmn2Service.getTasksDefinitions(deploymentUnit.getIdentifier(), processId);

Map<String, String> processData =
bpmn2Service.getProcessVariables(deploymentUnit.getIdentifier(), processId);

Map<String, String> taskInputMappings =
bpmn2Service.getTaskInputMappings(deploymentUnit.getIdentifier(), processId, "Write a Document" );
```

虽然它通常与其他服务(如部署服务)结合使用，但是它也可以单独使用，以获取有关不是来自kjar的流程定义信息。这可以通过使用定义服务的`builderProcessDefinition`方法来实现。


### 流程服务 (Process Service)

流程服务通常是最感兴趣的。一旦部署和定义服务已经被用来为系统提供可执行的东西。流程服务提供了访问了运行时的执行环境如：

- 启动一个新的流程实例
- 执行现有流程 - 发射信号时间，获取变量的信息。
- 与工作项目一起工作

同时流程服务是一个命令执行程序，因此它允许执行命令(基本上是ksession)来扩展其功能。

需要注意的是流程服务关注于运行时期的操作，因此只有需要更改流程实例(信号，变量)而不是用于读取操作如通过循环现实可用的流程实例然后执行`getProcessInstance`方法。为此，有专用的运行时数据服务(DataService),如下所述。

如何部署和运行流程的例子可以按如下方式完成：

```
KModuleDeploymentUnit deploymentUnit = new KModuleDeploymentUnit(GROUP_ID, ARTIFACT_ID, VERSION);

deploymentService.deploy(deploymentUnit);

long processInstanceId = processService.startProcess(deploymentUnit.getIdentifier(), "customtask");

ProcessInstance pi = processService.getProcessInstance(processInstanceId);
```

正如你看到的，启动流程需要`deploymentId`作为第一个参数。这非常强大，可以使服务轻松的与各种部署协同工作，即使采用相同的流程但是来自不同的版本，kjar版本。


### 运行时期数据服务 (Runtime Data Service)


运行时期数据服务正如其名，处理所有运行时期的信息：

- 已启动的流程实例
- 已执行的节点实例
- 更多

每当构建基于列表的用户界面是，是哟个此服务作为信息的主要来源 - 现实给定用户的流程定义，流程实例和任务等。次服务的设计尽可能高效,并仍提供所有必须的信息。一些例子：

- 获取所有流程定义

```
Collection definitions = runtimeDataService.getProcesses(new QueryContext());
```

- 获取活动的流程实例

```
Collection<processinstancedesc> activeInstances = runtimeDataService.getProcessInstances(new QueryContext());
```

- 获取给定流程实例的活动节点

```
List<Tasksummary> taskSummaries = runtimeDataService.getTasksAssignedAsPotentialOwner("john", new QueryFilter(0, 10));
```

运行时数据服务操作支持两个重要的参数：

- 查询上下文 (QueryContext)
- 查询过滤器 (QueryFilter) - 查询上下文的扩展

这些提供了高效管理结果集的功能，如分页(pagination),排序(sorting)以及顺序(ordering)。此外，额外的筛选可应用于任务查询，以在搜索用户任务时提供更高级的功能。


### 用户任务服务 (User Task Service)

用户任务服务涵盖个别任务的完整生命周期，因此可以从头至尾进行管理。它明确的消除了它的查询来提供范围执行并将所有查询操作移动到运行时数据服务中。除了生命周期操作之外，用户任务服务允许：

- 修改选定的属性
- 访问任务变量
- 访问任务附件(task attachments)
- 访问任务评论(task comments)

除此之外，用户任务服务也是一个命令执行程序他允许执行自定任务命令。完整的示例包含启动流程和通过任务服务完成用户任务：

```
long processInstanceId =
processService.startProcess(deployUnit.getIdentifier(), "org.jbpm.writedocument");

List<Long> taskIds =
runtimeDataService.getTasksByProcessInstanceId(processInstanceId);

Long taskId = taskIds.get(0);

userTaskService.start(taskId, "john");
UserTaskInstanceDesc task = runtimeDataService.getTaskById(taskId);

Map<String, Object> results = new HashMap<String, Object>();
results.put("Result", "some document data");
userTaskService.complete(taskId, "john", results);
```


> 使用服务是最重要的是，不再需要创建自己的流程服务实现，它只是简单的包装运行时管理其，运行时引擎和ksession的使用。服务是使用`RuntimeManager` API的最佳实践，从而消除使用该API时的各种风险。


### 基于Quartz的计时器服务 (Quartz-based TimerService)


jBPM通过Quartz提供了一个集群就绪的计时器服务，允许你随时处理或夹在知识会话。为了能正确的启动每个计时器，可以利用该服务来管理kie会话的TTL存活时间.

以下提供了集群环境中的基本Quartz配置文件：

```
#============================================================================
# Configure Main Scheduler Properties
#============================================================================

org.quartz.scheduler.instanceName = jBPMClusteredScheduler
org.quartz.scheduler.instanceId = AUTO

#============================================================================
# Configure ThreadPool
#============================================================================

org.quartz.threadPool.class = org.quartz.simpl.SimpleThreadPool
org.quartz.threadPool.threadCount = 5
org.quartz.threadPool.threadPriority = 5

#============================================================================
# Configure JobStore
#============================================================================

org.quartz.jobStore.misfireThreshold = 60000

org.quartz.jobStore.class=org.quartz.impl.jdbcjobstore.JobStoreCMT
org.quartz.jobStore.driverDelegateClass=org.quartz.impl.jdbcjobstore.StdJDBCDelegate
org.quartz.jobStore.useProperties=false
org.quartz.jobStore.dataSource=managedDS
org.quartz.jobStore.nonManagedTXDataSource=nonManagedDS
org.quartz.jobStore.tablePrefix=QRTZ_
org.quartz.jobStore.isClustered=true
org.quartz.jobStore.clusterCheckinInterval = 20000

#============================================================================
# TODO: Configure Datasources
#============================================================================
#org.quartz.dataSource.managedDS.jndiURL=
#org.quartz.dataSource.nonManagedDS.jndiURL=
For more information on configuring a Quartz scheduler, please see the documentation for the 1.8.5 distribution archive.
```


### 查询服务 (QueryService)

查询服务提供了基于Dashbuilder DataSet的高级查询功能。其背后的概念是，用户可以控制如何从底层数据存储中检索数据。这包含与外部标的复杂链接，如JPA实体表，自定义系统数据库表等。

查询服务有两个部分组成：

- 管理操作
  - 注册查询定义
  - 替换查询定义
  - 注销(删除）查询定义
  - 获取查询定义
  - 获取所有已注册的查询定义

- 运行时操作
  - 有两种风格的查询
    - 简单基于`QueryParam`作为过滤器提供者
    - 基于`QueryParamBuilder`的高级过滤器提供者

DashBuilder DataSets在加BPM中为多个数据源(CSV, SQL, elastic search等)提供了支持 - 因为它的后端是基于RDBMS的专注于基于SQL的数据集。所以jBPM中的QueryService是DashBuilder DataSets功能的一个子集，可以通过简单的API实现高效的查询。


**术语 (Terminology)**

- **查询定义(QueryDefinition)** - 用于表示包含唯一的名称、sql表达式以及该查询的y源地址(执行查询时使用的数据源的JNDI名称)的数据集(data set)的定义。
- **查询参数(QueryParam)** - 表示单一查询参数的基本数据结构 - 条件 - 包含： 列(column), 名称(name), 运算符(operator)以及预期的值(expected values)
- **查询结果映射(QueryResultMapper)** - 负责将原始的结果数据集(行和列)映射成对象的形式
- **查询参数构造器(QueryParamBuilder)** - 负责构建将被应用到给定查询的查询定义的查询过滤器


由于`QueryDefinition`和`QueryParam`非常简单，而`QueryParamBuilder`和`QueryResultmapper`可能有点复杂而则更多需要注意的是正确使用它们以及使用他们的特性。


**查询结果映射(QueryResultMapper)**


映射正如其名，将从数据库中提取的数据映射成对象表现形式。非常像对象关系映射(ORM)提供商，比如hibernate将表映射成实体。显然，可能有许多对象类型可用于表示数据集结果，所以几乎不可能开箱即用提供她们。映射器非常强大，因为可以插拔，你可以实现自己的映射器，将结果转换成你喜欢的任何类型。jBPM提供了以下开箱即用的映射器：

- `org.jbpm.kie.services.impl.query.mapper.ProcessInstanceQueryMapper` - 注册名称为`ProcessInstances`.
- `org.jbpm.kie.services.impl.query.mapper.ProcessInstanceWithVarsQueryMapper` - 注册名称为`ProcessInstancesWithVariables`
- `org.jbpm.kie.services.impl.query.mapper.ProcessInstanceWithCustomVarsQueryMapper` - 注册名称为`ProcessInstancesWithCustomVariables`
- `org.jbpm.kie.services.impl.query.mapper.UserTaskInstanceQueryMapper` - 注册名称为 `UserTasks`
- `org.jbpm.kie.services.impl.query.mapper.UserTaskInstanceWithVarsQueryMapper` - 注册名称为`UserTasksWithVariables`
- `org.jbpm.kie.services.impl.query.mapper.UserTaskInstanceWithCustomVarsQueryMapper` - 注册名称为`UserTasksWithCustomVariables`
- `org.jbpm.kie.services.impl.query.mapper.TaskSummaryQueryMapper` - 注册名称为`TaskSummaries`
- `org.jbpm.kie.services.impl.query.mapper.RawListQueryMapper` - 注册名称为`RawList`

每个`QueryResultMapper`都可以用给定的名称进行注册，一允许通过名称进行简单查找，而不是引用其类名称 - 当我们想要减少依赖关系的数量并因此不以来于客户端的是现实时，使用EJB风格的服务尤其重要。所以为了能够通过名称引用`QueryResultMapper`因改使用`NamedQueryMapper`它是`jbpm-services-api`的一部分。它充当委托(懒式的委托)因为只有在实际查询执行时才查找实际的映射器。

```
queryService.query("my query def", new NamedQueryMapper<Collection<ProcessInstanceDesc>>("ProcessInstances"), new QueryContext());
```

**QueryParamBuilder**


`QueryParamBuilder`为我们的数据集提供了更多的高级方式去构建过滤器。默认情况下使用接受0个或多个`QueryParam`实例的`QueryService`的查询方法(正如我们在上面的例子看到的)所有这些参数将与`AND`运算符连在一起。但情况并非总是如此，所以这就是为什么`QueryParamBuilder`已经引入用户构建他们的构建器的原因，这些构建起将在查询发布时提供过滤器。


有一个可用的`QueryParamBuilder`，它用于覆盖基于所谓核心函数的默认`QueryParam`s。这些核心功能是基于SQL的条件，包括一下内容：

- IS_NULL
- NOT_NULL
- EQUALS_TO
- NOT_EQUALS_TO
- LIKE_TO
- GREATER_THAN
- GREATER_OR_EQUALS_TO
- LOWER_THAN
- LOWER_OR_EQUALS_TO
- BETWEEN
- IN
- NOT_IN

`QueryParamBuilder`是一个简单的接口，只要其构建方法在执行查询之前返回为非空值就会被调用。所以你能够构建不能简单使用`QueryParam`s来表示的复杂的过滤器选项。这里是`QueryParamBuilder`的基本实现，让你可以开始实现自己的功能 - 注意它依赖与DashBuilder数据集API。

```
public class TestQueryParamBuilder implements QueryParamBuilder<ColumnFilter> {

    private Map<String, Object> parameters;
    private boolean built = false;
    public TestQueryParamBuilder(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    @Override
    public ColumnFilter build() {
        // return null if it was already invoked
        if (built) {
            return null;
        }

        String columnName = "processInstanceId";

        ColumnFilter filter = FilterFactory.OR(
                FilterFactory.greaterOrEqualsTo((Long)parameters.get("min")),
                FilterFactory.lowerOrEqualsTo((Long)parameters.get("max")));
        filter.setColumnId(columnName);

        built = true;
        return filter;
    }
}
```


**典型的使用场景**

用户需要做的第一件事就是定义数据集 - 查看要使用的数据 - 在服务api中定义为`QueryDefinition`。

```
// 定义查询定义
SqlQueryDefinition query = new SqlQueryDefinition("getAllProcessInstances", "java:jboss/datasources/ExampleDS");
query.setExpression("select * from processinstancelog");
```

这是最简单的查询定义，因为它可以是：

- 构造函数需要
  - 在运行时一个唯一的标识名字
  - 数据源JNDI名字用于在这个查询定义上执行查询 - 换句话说就是数据的来源

- 表达式 (Expression) - 最主要的部分 - 当执行查询时被用来构建过滤后的视图的sql语句

一旦我们有了SQL查询定义，我们就可以注册它，以便以后可以用于实际查询。

```
queryService.registerQuery(query)
```

从现在开始，这个查询定义可以用来执行实际的查询(或数据查找来使用数据集中的术语)。以下是无需过滤即可收集数据的基本功能:

```
Collection<ProcessInstanceDesc> instances = queryService.query("getAllProcessInstances", ProcessInstanceQueryMapper.get(), new QueryContext());
```

上面的查询非常简单，并使用`QueryContext`的默认值 - 分页和排序。所以让我们看看改变分页和排序默认值的一个：

```
QueryContext ctx = new QueryContext(0, 100, "start_date", true);

Collection<ProcessInstanceDesc> instances = queryService.query("getAllProcessInstances", ProcessInstanceQueryMapper.get(), ctx);
```

现在我们来看看如何进行数据过滤：

```
// single filter param
Collection<ProcessInstanceDesc> instances = queryService.query("getAllProcessInstances", ProcessInstanceQueryMapper.get(), new QueryContext(), QueryParam.likeTo(COLUMN_PROCESSID, true, "org.jbpm%"));

// multiple filter params (AND)
Collection<ProcessInstanceDesc> instances = queryService.query("getAllProcessInstances", ProcessInstanceQueryMapper.get(), new QueryContext(),
 QueryParam.likeTo(COLUMN_PROCESSID, true, "org.jbpm%"),
 QueryParam.in(COLUMN_STATUS, 1, 3));
```

随着最终用户操控定义数据以及该如何获取数据。不受JPA提供者的限制，也不受其他任何限制。此外，这促进了为你的环境量身定做的查询，因为在大多数情况下会使用单个数据库，因此可以使用该数据库的特定功能来提升性能。


